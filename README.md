Задача для обучения. Мониторинг приложения в Docker контейнере с помощью Grafana, Prometeus, Loki.

Light:
1) Создать ВМ для сервиса (app) и ВМ для будущей системы мониторинга (monitoring) Использовал Vagrant (DONE)
2) Установить docker и docker-compose и запустить приложение через docker-compose (DONE)
3) Установить сервисы Grafana, Loki, Prometeus на monitoring (DONE)
4) Освоить dockerfile-инструкцию HEALTHCHECK и добавить  проверку в проект. (DONE)
5) Установить на app prometeus-exporter и настроить сбор метрик на работоспособность контейнеров. (DONE)
6) Виртуализировать метрики с помощью Grafana. (DONE)

Normal:
1) Написать Ansible Playbook для развёртывания мониторинга. (PROGRESS)
2) Написать Ansible Playbook для установки проекта. (PROGRESS)
3) Вынести в отдельную ansible role установку мониторинга. (PROGRESS)
4) Написать отдельную роль для Prometeus-exporter и настроить сбор метрик на работоспособность контейнеров. (PROGRESS)

Hard:
1) Развернуть в проект в облачном Kubernetus (PROGRESS)
2) Покрыть роли тестами Molecule и автоматизировать тесты с помощью Gitlab CI/CD  (PROGRESS)
3) Добавить несколько видов алертинга на ваше усмотрение (почта, телеграмм, звонок)  (PROGRESS)

Используемый стек: Grafana v9.2.3, Prometeus v2.39.1, Loki v2.6.0, Docker v20.10.7, Docker-compose v1.23.2, Ansible v2.13.5, Molecule (Драйвер Docker) v4.0.3, Vagrant v2.3.0.